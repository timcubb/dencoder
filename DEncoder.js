/*

@file: Dencoder.js
@author: http://bitbucket.org/timcubb / http://github.com/timcubb
@copyright: 2013 

@description: functions to encode and decode messages. Make for a 
secure chatting app so intercepted messages are meanningless unless 
you have the key, which should be sent in a different request.

@usage:

var input = "This is an important, private message.";

var encoded = Dencoder.encode_(input); // (OR use encode_sort(input);
	-> value of encoded.output: "      ,.Taaaaeeeghiiiimmnnopprrsssstttv"
	-> value of encoded.key: "10,21,29,4,7,20,37,0,17,26,34,8,28,31,36,35,1,....."

var decoded = Dencoder.decode(encoded.output, encoded.key);
	-> value of decoded: "This is an important, private message."

*/

// Namespace
var Dencoder = {};

Dencoder.encode_sort = function(str) {

	var total_size = str.length, 
		i, x, t, key = [], scrambled = [],
		output = [];

	for(i=0; i<total_size; i++) {
		var t = str.substring(i,i+1);
		
		output.push(t+'|'+i);
	}
	output=output.sort();
	output.forEach(function(k){
		var temp = k.split('|');
		key.push(temp[1]);
		scrambled.push(temp[0]);
	});

	return { output: Dencoder.utils.glue(scrambled), key: key };
}

Dencoder.encode = function(str) {

	var total_size = str.length, 
		i, x,
		output = [],
		unlock_key,
		scrambled = [];

	for(i=0; i<total_size; i++) {
		var temp = str.substring(i,i+1);
		output.push(temp);
	}

	var q, z, asize = output.length,
		rand_key = Dencoder.utils.get_random_key(asize);

	for(z=0, q=asize; z<q; z++) {
		var rk = rand_key[z];
		scrambled[z] = output[rk];
	}

	return { output: Dencoder.utils.glue(scrambled), key: rand_key };
};


Dencoder.decode = function(arr, key) {

	if(arr.length != key.length) {
		return "not a valid key!";
	}

	var  i, o = [], t, total_size = arr.length;

	for(i=0; i<total_size; i++) {
		var t = arr.substring(i,i+1);
		o[i]=t;
	}

	var total_size = o.length, i, x, output = [], key = Dencoder.utils.invert_array(key);

	for(i=0, x=total_size; i<x; i++) {

		var ul = key[i];
		output[i] = o[ul];
	}

	return Dencoder.utils.glue(output);
};


// utility Functions

Dencoder.utils = {};

Dencoder.utils.get_random_key = function(length) {
	var cache = [], 
		i, output = [], t;

	for(i=0;i<length;i++) {
		t = Dencoder.utils.new_random(length, output);
		output.push(t);
	}
	return output;
};


Dencoder.utils.new_random = function(length, cache) {
	var x = 1;
	while(x>0) {
		t = Math.floor(Math.random()*length);
		if(!Dencoder.utils.in_cache(cache, t)){
			return t;
		}
	}
};


Dencoder.utils.in_cache = function(arr, str) {
	if(arr.indexOf(str) != -1) {
		return true;
	}
	return false;
};


Dencoder.utils.invert_array = function(arr) {
	var i,k,v,o = [];
	for(i=0,x=arr.length;i<x;i++) {
		k = i;
		v = arr[k];
		o[v] = k;
	}
	return o;
};

Dencoder.utils.glue = function(arr) {
	return arr.join('');;
};
