
# DEncoder.js

DEncoder.js is a little js utility I created for encoding and decoding short messages
originally for a secure chat server. If any of the messages are intercepted by a thirdparty,
the message is completely useless unless they have the dynamic key (you can encode 8 of the same 
message and each key will be different. [Except with encode_sort])

## Usage:

	var input = "Hey man whats going on? I havent seen you in forever bro, where you been?";

	// Random encoder
	var encoded = Dencoder.encode(input);

	-> encoded.output: "um  aes aybn  e nhyueew wer yr ohorvgbgfio e?esiernahoI?o vn, nH nnteto e "
	-> encoded.key: 66,4,52,13,27,69,33,57,5,2,68,21,23,44,70,63,36,59,64,40,60,1,8,3,58,35,54,...

OORR:

	// Sorted encoder
	var encoded = Dencoder.encode_sort(input);

	-> encoded.output: "             ,??HIaaabbeeeeeeeeeefgghhhiimnnnnnnnoooooorrrrssttuuvvwwyyy"
	-> encoded.key: 13,19,23,25,3,32,37,41,44,52,57,63,67,7,56,22,72,0,24,10,27,5,53,68,...

The decode method can be used to decode both encode, and encode_sort encoded messages, as long as 
the key is provided.

	// Decode 
	var decoded = Dencoder.decode(encoded.output, encoded.key);
	-> decoded: "Hey man whats going on? I havent seen you in forever bro, where you been?"


Copyright: 2013, timcubbedhe@gmail.com, No Rights Reserved.

http://github.com/timcubb
http://bitbucket.org/timcubb


## Test

Checkout the test.html file for a simple demonstration:


	var input = "This is an important, private message.";

	console.log("----> Encoding");
	var encoded = Dencoder.encode_sort(input);

	console.log("--> Encoded Output: "+encoded.output);
	console.log("--> Key: "+encoded.key);

	console.log("----> Decoding");
	var decoded = Dencoder.decode(encoded.output, encoded.key);

	console.log("--> Decoded Output: "+decoded);


The output in the console (Or if you uncomment the line in the test.html file, you can see the output in the page body instead)


	----> Encoding
	--> Encoded Output:      ,.Taaaaeeeghiiiimmnnopprrsssstttv
	--> Key: 10,21,29,4,7,20,37,0,17,26,34,8,28,31,36,35,1,11,2,24,5,12,30,18,9,14,13,22,15,23,3,32,33,6,16,19,27,25
	----> Decoding
	--> Decoded Output: This is an important, private message. 


## License

DEncoder is licensed under the DWTFYW license (Do Whatever The Fuck You Want). You can use this code in whatever 
way you want, but I am not and cannot be responsible for any damages caused by using this sofware. This software 
is provided AS-IS only!! Use at your own risk. If you have any questions or pull requests feel free to contact me.


